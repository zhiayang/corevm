// Main.cpp
// Copyright (c) 2014, zhiayang@gmail.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

#include <stdio.h>
#include <assert.h>
#include <vector>
#include <sys/stat.h>
#include "Assembler.hpp"
#include "Utilities.hpp"

using namespace std;
#define OUTPUT_NAME ".__x_cpped"

int main(int argc, char** argv)
{
	char* outputname = 0;
	if(argc < 2)
	{
		printf("Error: no input files.");
		exit(1);
	}

	int i = 1;
	int f = 0;
	for(i = 1; i < argc; i++)
	{
		if(!strcmp(argv[i], "-o"))
		{
			i++;
			f++;
			outputname = new char[256];
			strncpy(outputname, argv[i], 256);
		}
		f++;
	}

	if(!outputname)
	{
		outputname = new char[256];

		string s = string(argv[f]);
		StringUtil::ReplaceString(s, "crs", "cre");
		strncpy(outputname, s.c_str(), 256);
	}






	FILE* unprocessed = fopen(argv[f], "r");

	if(!unprocessed)
	{
		printf("Error: file '%s' does not exist.\n", argv[f]);
		exit(1);
	}

	// invoke 'cpp'
	system(StringUtil::Format("cpp %s %s.crs", argv[f], OUTPUT_NAME).c_str());

	// read the file ".__x_cpped.crs"
	FILE* input = fopen(StringUtil::Format("%s.crs", OUTPUT_NAME).c_str(), "r");

	if(!input)
	{
		printf("Error: preprocessing failed, '%s' does not exist.\n", OUTPUT_NAME);
		exit(1);
	}

	struct stat st;
	stat(OUTPUT_NAME ".crs", &st);
	int64_t size = st.st_size;

	Assembler::InitialiseOpcodeTable();
	vector<Assembler::DataBytes> data;
	vector<Assembler::Operation>* ops = Assembler::Parse(input, (uint64_t) size, &data);
	Assembler::Serialise(ops, outputname, &data);

	fclose(input);
	fclose(unprocessed);
}






