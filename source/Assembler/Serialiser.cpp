// Serialiser.cpp
// Copyright (c) 2014, zhiayang@gmail.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

#include <vector>
#include <fstream>
#include <iostream>
#include <assert.h>
#include <Assembler.hpp>

#include "CoreExecFormat.hpp"

using namespace std;

namespace Assembler
{
	void Write8(uint8_t* buf, uint64_t offset, uint8_t val)
	{
		buf[offset] = val;
	}

	void Write16(uint8_t* buf, uint64_t offset, uint16_t val)
	{
		*((uint16_t*) (buf + offset)) = val;
	}

	void Write32(uint8_t* buf, uint64_t offset, uint32_t val)
	{
		*((uint32_t*) (buf + offset)) = val;
	}

	void Write64(uint8_t* buf, uint64_t offset, uint64_t val)
	{
		*((uint64_t*) (buf + offset)) = val;
	}


	void Serialise(vector<Operation>* ops, string name, vector<DataBytes>* outdata)
	{
		FILE* out = fopen(name.c_str(), "w");

		// do assertions
		Operation tmp = ops->front();
		assert(sizeof(tmp.opcode) == 2);
		assert(sizeof(tmp.input1) == 1);
		assert(sizeof(tmp.input2) == 1);
		assert(sizeof(tmp.output) == 1);
		assert(sizeof(tmp.immediate) == 8);

		// need to setup an ELF file.
		// TODO: ELF.

		uint8_t* buffer = new uint8_t[ops->size() * 16];
		memset(buffer, 0, ops->size() * 16);

		// loop through operations.
		uint64_t offset = 0;
		uint64_t entry = 0;




		uint64_t execsize = ops->size() * 16;
		uint64_t datasize = 0;

		for(auto d : *outdata)
			datasize += d.length;



		// setup the vxef file. (extra for padding)
		uint8_t* vxef = new uint8_t[sizeof(CoreHeader) + execsize + datasize];

		CoreHeader* header = (CoreHeader*) vxef;
		memset(vxef, 0, sizeof(CoreHeader) + execsize + datasize);

		header->Signature[0] = 'c';
		header->Signature[1] = 'o';
		header->Signature[2] = 'r';
		header->Signature[3] = 'e';
		header->ExecutableLoadAddr = 0x00100000;

		header->ExecutableSize = execsize;
		header->EntryPoint = header->ExecutableLoadAddr + entry;

		header->DataSize = datasize;
		header->DataLoadAddr = header->ExecutableLoadAddr + execsize;


		for(Operation op : *ops)
		{
			if(op.opcode == 0xDEAD)
			{
				entry = op.immediate;
			}
			else
			{
				// byte 7 (offset + 6) specifies which operand uses the immediate value.
				// byte 8 (offset + 7) specifies whether the output operand is a dereference op ie. brackets.
				// TODO: if value is 0, there is no immediate value.
				// TODO: compress bytecode

				Write16(buffer, offset, op.opcode);
				Write8(buffer, offset + 2, op.input1);
				Write8(buffer, offset + 3, op.input2);
				Write8(buffer, offset + 4, op.output);
				Write8(buffer, offset + 5, op.immsign);
				Write8(buffer, offset + 6, op.immpos);
				Write8(buffer, offset + 7, op.deref_sizeprefix);

				if(op.immpos > 0)
				{
					Write64(buffer, offset + 8, (op.dataderef ? header->DataLoadAddr : 0) + op.immediate);
					offset += 16;
				}
				else
					offset += 8;
			}
		}


		// copy to the buffer.
		memcpy(vxef + sizeof(CoreHeader), buffer, execsize);
		delete[] buffer;

		uint64_t runningtotal = (uintptr_t) vxef + execsize;
		for(auto m : *outdata)
		{
			memcpy((void*) runningtotal, m.raw, m.length);
			runningtotal += m.length;
		}

		fwrite(vxef, 1, sizeof(CoreHeader) + execsize + datasize, out);
		fflush(out);
		fclose(out);
	}
}















