// Tokeniser.cpp
// Copyright (c) 2014, zhiayang@gmail.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

#include <cmath>
#include <iostream>
#include <string>
#include "Assembler.hpp"
#include "Utilities.hpp"

using namespace std;
namespace Assembler
{
	string& SkipWhitespace(string& line)
	{
		int i = 0;
		while(i < line.length() && isspace(line[i]))
			line = line.substr(1);

		return line;
	}

	Token& GetNextToken(std::string& line)
	{
		Token* tok = new Token();
		memset(tok, 0, sizeof(Token));

		// skip whitespace
		line = SkipWhitespace(line);

		// get the first char.
		// if it's a '$', expect an immediate.
		// if it's a '%', expect a register.

		if(line.length() > 0)
		{
			if(line[0] == '$')
			{
				line = line.substr(1);

				// check if we're not doing a number
				if(!isdigit(line[0]) && line[0] != '-')
				{
					string nm = ParseWord(line, ' ', true);

					tok->type = IType::DataReference;
					tok->labelname = new string(nm);
				}
				else
				{
					size_t invalid = 0;
					tok->type = IType::Imm;
					bool isneg = line[0] == '-';

					try
					{
						tok->value = isneg ? std::stoll(line, &invalid, 0) : std::stoull(line, &invalid, 0);
					}
					catch(std::exception)
					{
						Error("Error parsing number", linenum);
					}

					line = line.substr(invalid);
				}
			}
			else if(line[0] == '%')
			{
				tok->type = IType::Reg;
				if(line[1] == 'r')
				{
					int chs = 4;

					// manual parsing.
					if(line[2] == 'r')
					{
						tok->value = ResultRegisterIndex;
						chs = 3;
					}
					else if(line[2] == 'i' && line[3] == 'p')
					{
						tok->value = InstructionPointerIndex;
					}
					else if(line[2] == 's' && line[3] == 'p')
					{
						tok->value = StackPointerIndex;
					}
					else if(line[2] >= '0' && line[2] <= '3' && line[3] >= '0' && line[3] <= '9')
					{
						tok->value = ((line[2] - '0') * 10) + (line[3] - '0');

						if(tok->value > 32)
							Error(StringUtil::Format("Malformed register name %%r%lld", tok->value), linenum);
					}
					else
						Error(StringUtil::Format("Malformed register name %%r%lld", tok->value), linenum);

					// registers are 4 chars wide
					line = line.substr(chs);
				}
				else if(line[1] == 'a')
				{
					if(line[2] >= '0' && line[2] <= '1' && line[3] >= '0' && line[3] <= '9')
					{
						tok->value = 32 + ((line[2] - '0') * 10) + (line[3] - '0');

						if(tok->value > 16 + 32)
							Error(StringUtil::Format("Malformed register name %%r%lld", tok->value), linenum);
					}
					else
						Error(StringUtil::Format("Malformed register name %%r%lld", tok->value), linenum);

					// registers are 4 chars wide
					line = line.substr(4);
				}
				else if(line[1] == 'v')
				{
					if(line[2] >= '0' && line[2] <= '3' && line[3] >= '0' && line[3] <= '9')
					{
						tok->value = 64 + ((line[2] - '0') * 10) + (line[3] - '0');

						if(tok->value > 32 + 64)
							Error(StringUtil::Format("Malformed register name %%r%lld", tok->value), linenum);
					}
					else
						Error(StringUtil::Format("Malformed register name %%r%lld", tok->value), linenum);

					// registers are 4 chars wide
					line = line.substr(4);
				}
				else
				{
					Error(StringUtil::Format("Malformed register name"), linenum);
				}
			}
			else if(line[0] == '-')
			{
				if(line[1] == '>')
				{
					tok->type = IType::Arrow;
					line = line.substr(2);
				}
				else
				{
					Error(StringUtil::Format("Rogue '-' character"), linenum);
				}
			}
			else if(line[0] == '<')
			{
				if(line[1] == '-')
				{
					tok->type = IType::LeftArrow;
					line = line.substr(2);
				}
				else
				{
					Error(StringUtil::Format("Rogue '<' character"), linenum);
				}
			}
			else if(line[0] == '.')
			{
				line = line.substr(1);

				tok->labelname = new string();
				for(int i = 0; i < line.length() && isalnum(line[i]); i++)
				{
					tok->labelname->push_back(line[i]);
				}

				tok->type = IType::Label;
			}
			else if(line[0] == '@')
			{
				int num = 0;
				line = line.substr(1);

				tok->labelname = new string();
				for(int i = 0; i < line.length() && isalnum(line[i]); i++, num++)
				{
					tok->labelname->push_back(line[i]);
				}

				line = line.substr(num);

				tok->type = IType::Function;
			}
			else if(line[0] == '(')
			{
				line = line.substr(1);
				tok->type = IType::LBracket;
				tok->deref = true;
			}
			else if(line[0] == ')')
			{
				line = line.substr(1);
				tok->type = IType::RBracket;
			}
			// what the fuck am I, a newb c++ programmer?
			else if(line[0] == 'b' && line[1] == 'y' && line[2] == 't' && line[3] == 'e')
			{
				line = line.substr(4);
				tok->type = IType::SizePrefix;
				tok->value = 1;
			}
			else if(line[0] == 'd' && line[1] == 'o' && line[2] == 'u' && line[3] == 'b' && line[4] == 'l' && line[5] == 'e')
			{
				line = line.substr(6);
				tok->type = IType::SizePrefix;
				tok->value = 2;
			}
			else if(line[0] == 'q' && line[1] == 'u' && line[2] == 'a' && line[3] == 'd')
			{
				line = line.substr(4);
				tok->type = IType::SizePrefix;
				tok->value = 4;
			}
			else if(line[0] == 'o' && line[1] == 'c' && line[2] == 't' && line[3] == 'o')
			{
				line = line.substr(4);
				tok->type = IType::SizePrefix;
				tok->value = 8;
			}
			else if(line[0] == 'r' && line[1] == 'e' && line[2] == 'l')
			{
				line = line.substr(3);
				tok->type = IType::SizePrefix;
				tok->value = 0xFFFF;
			}
			else if(line[0] == '\"')
			{
				line = line.substr(1);
				tok->labelname = new string();

				int max = line.length();
				int i = 0;
				while(line[i] != '\"')
				{
					if(line[i] == 0 || i >= max)
						Error("Expected end of string", linenum);

					*tok->labelname += line[i];
					i++;
				}
			}
		}
		else
		{
			tok->type = IType::None;
		}

		// this function WILL mutate the argument.
		// skip whitespace and commas.
		{
			line = SkipWhitespace(line);

			// remove the comma
			if(line[0] == ',')
			{
				line = line.substr(1);
			}
			else if(line[1] == '-' && line[2] == '>')
			{
				line = line.substr(2);
			}

			// remove whitespace again
			line = SkipWhitespace(line);
		}

		return *tok;
	}
}

