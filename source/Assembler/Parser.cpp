// Parser.cpp
// Copyright (c) 2014, zhiayang@gmail.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

#include <stdio.h>
#include <assert.h>
#include <vector>
#include <map>
#include <cmath>
#include <string>
#include <stdexcept>
#include <sys/stat.h>
#include <algorithm>

#include "Assembler.hpp"
#include "Utilities.hpp"
#include "VM.hpp"

using namespace std;
namespace Assembler
{
	map<string, Instruction>* OpcodeMap;
	map<string, int64_t>* Labels = 0;
	map<string, int64_t>* Functions = 0;
	map<string, uint64_t>* Data = 0;
	uint64_t linenum = 1;
	uint64_t instrptr = 0;
	uint64_t funcfp = 0;
	int curpass = 0;

	// 0 = code, 1 = data
	int mode = 0;

	uint64_t dataoffset = 0;

	std::string ParseWord(std::string& line, char stop, bool isnewlineokay)
	{
		std::string ret;
		while(line[0] != stop)
		{
			if(line.length() == 0 && isnewlineokay)
				break;

			else if(line.length() == 0)
				Error("Malformed directive", linenum);


			ret += line[0];
			line = line.substr(1);
		}

		return ret;
	}

	void ParseData(std::string& line, std::vector<DataBytes>* outdata)
	{
		// format: name size data.
		// parse until space.
		string nm;
		int size = 0;

		nm = ParseWord(line);

		assert(line[0] == ' ');
		line = line.substr(1);

		string sz = ParseWord(line);
		if(sz == "byte")		size = 1;
		else if(sz == "double")	size = 2;
		else if(sz == "quad")	size = 4;
		else if(sz == "octo")	size = 8;
		else if(sz == "ascir")	size = 0;	// raw ascii, no null terminator
		else if(sz == "ascii")	size = -1;	// ascii + terminator
		else			Error(StringUtil::Format("Unknown size %s", sz.c_str()), linenum);

		if(line.length() < 2)
			Error("Expected data after such a directive", linenum);

		if(line[0] == ' ')
			line = line.substr(1);

		// these are numerical constants.
		if(size > 0)
		{
			uint64_t value = 0;
			size_t invalid = 0;

			// make sure we have the '$' prefix (unlike GNU AS)
			if(line[0] != '$')
				Error("Expected '$' before constant", linenum);

			line = line.substr(1);
			bool isneg = line[0] == '-';

			try
			{
				value = isneg ? std::stoll(line, &invalid, 0) : std::stoull(line, &invalid, 0);
			}
			catch(std::invalid_argument)
			{
				Error("Failed to parse number", linenum);
			}
			catch(std::out_of_range)
			{
				Error("Given value was too large for any specifier", linenum);
			}

			line = line.substr(invalid);
			if(line.length() > 1)
				Error(StringUtil::Format("Unexpected garbage \"%s\" after data directive", line.c_str()), linenum);

			// make a new bunch of bytes, and copy bitwise (memcpy).
			if(size == 1 && value > 0xFF)			Error(StringUtil::Format("Value %lld was too large for 'byte' size specifier", value), linenum);
			if(size == 2 && value > 0xFFFF)			Error(StringUtil::Format("Value %lld was too large for 'double' size specifier", value), linenum);
			if(size == 4 && value > 0xFFFFFFFF)		Error(StringUtil::Format("Value %lld was too large for 'quad' size specifier", value), linenum);
			if(size == 8 && value > 0xFFFFFFFFFFFFFFFF)	Error(StringUtil::Format("Value %lld was too large for 'octo' size specifier", value), linenum);

			uint8_t* dat = new uint8_t[size];
			memcpy(dat, &value, size);

			outdata->push_back(DataBytes { dat, (size_t) size });
			(*Data)[nm] = dataoffset;

			dataoffset += size;
		}
		else
		{
			// expect a string. (todo)
		}
	}

	vector<Operation>* Parse(FILE* input, uint64_t bytes, std::vector<DataBytes>* outdata)
	{
		vector<Operation>* ops = new vector<Operation>();

		if(!Labels || !Functions || !Data)
		{
			Labels = new map<string, int64_t>();
			Functions = new map<string, int64_t>();
			Data = new map<string, uint64_t>();
		}

		uint64_t totalbytes = 0;

		// loop twice, once to get the functions and labels, another to actually parse the instructions.
		for(int pass = -1; pass < 2; pass++)
		{
			curpass = pass;
			while(totalbytes < bytes)
			{
				// read and terminate string, replace \n with 0.
				size_t readbytes = 0;
				char* l = fgetln(input, &readbytes);
				if(!l)
					break;

				if(l[0] == '\n')
				{
					totalbytes++;
					linenum++;
					continue;
				}


				l[readbytes - 1] = 0;
				string line = string(l);

				// trim whitespace
				Utilities::TrimString(line);

				// ignore comments, preprocessed things and directives.
				if((line[0] == '/' && line[1] == '/') || line[0] == '#')
				{
					linenum += (line[0] == '#' ? 0 : 1);
					totalbytes += line.length();
					continue;
				}
				else if(line[0] == '[')
				{


					line = line.substr(1);
					string dirname = ParseWord(line, ']');

					// todo: better directive search
					if(dirname == "code")
						mode = 0;

					else if(dirname == "data")
						mode = 1;

					else
						Error(StringUtil::Format("Unrecognised directive [%s]", dirname.c_str()), linenum);



					linenum++;
					totalbytes += line.length();
					continue;
				}

				// transform to lowercase, reduce troubles
				std::transform(line.begin(), line.end(), line.begin(), ::tolower);




				// check for labels here, so we can skip processing.
				if(line[0] == '.')
				{
					line = line.substr(1);

					if(pass == 0)
					{
						if(Labels->find(line) != Labels->end())
							Error(StringUtil::Format("Duplicate label %s", line.c_str()), linenum);

						(*Labels)[line] = instrptr;
					}
				}
				else if(line[0] == '@')
				{
					line = line.substr(1);

					if(pass == 0)
					{
						if(Functions->find(line) != Functions->end())
							Error(StringUtil::Format("Duplicate function %s", line.c_str()), linenum);

						(*Functions)[line] = instrptr;
						if(line == "main")
							ops->insert(ops->begin(), Operation { 0xDEAD, instrptr, 0, 0, 0, 0 });
					}
				}
				else
				{
					if(mode == 0 && pass >= 0)
					{
						// send to parseline.
						if(pass > 0)
							ops->push_back(ParseLine(line));


						if((pass > 0 ? ops->back() : ParseLine(line)).immpos > 0)
							instrptr += 16;

						else
							instrptr += 8;
					}
					else if(mode == 1 && pass == -1)
					{
						ParseData(line, outdata);
					}
				}

				totalbytes += readbytes;
				linenum++;
			}

			rewind(input);
			totalbytes = 0;
			instrptr = 0;
			linenum = 1;
			mode = 0;
			dataoffset = 0;

			if(pass > 0 && (ops->size() < 1 || (*ops)[0].opcode != 0xDEAD))
				Error(StringUtil::Format("Missing @main function, aborting..."), 0);
		}
		return ops;
	}

	// helper func, checks if t2 is a valid kind of t1.
	// t2 is the parsed result, so it cannot be 'either', or 'optional'.
	bool IsValidType(IType t1, IType t2)
	{
		// todo: better way. (bitmasks?)
		if(t1 == t2)
			return true;

		else if((t1 == IType::OpReg || t1 == IType::Reg) && t2 == IType::Reg)
			return true;

		else if((t1 == IType::OpImm || t1 == IType::Imm) && t2 == IType::OpImm)
			return true;

		else if(t1 == IType::MemReg && (t2 == IType::Mem || t2 == IType::DataReference || t2 == IType::Reg))
			return true;

		else if(t1 == IType::MemImm && (t2 == IType::Mem || t2 == IType::DataReference || t2 == IType::Imm))
			return true;

		else if(t1 == IType::MemRegImm && (t2 == IType::Mem || t2 == IType::DataReference || t2 == IType::Imm || t2 == IType::Reg))
			return true;

		else if(t1 == IType::Either)
			return IsValidType(IType::Reg, t2) || IsValidType(IType::Imm, t2);

		return false;
	}

	bool ParseJumpToAddress(string& line, Operation* op, Token& curtok, string& mnemonic, bool isrel)
	{
		// accept a label, an immediate or a register dereference.
		if(curtok.type == IType::Imm && !isrel)
		{
			op->immediate = curtok.value;
			op->immpos = 1;
			return true;
		}
		else if(curtok.type == IType::Imm && isrel)
		{
			op->immediate = curtok.value;
			op->immpos = 4;
			return true;
		}
		else if(curtok.type == IType::LBracket)
		{
			curtok = GetNextToken(line);
			if(curtok.type != IType::Reg)
				Error("Expected register after dereference bracket", linenum);

			op->input2 = curtok.value;
			op->deref_sizeprefix |= 0x80;
			op->immpos = isrel ? 4 : 0;
			return true;
		}
		else
			return false;
	}

	void ParseJumpToLabel(string& line, Operation* op, string& mnemonic, bool isrel)
	{
		Token curtok = GetNextToken(line);
		if(ParseJumpToAddress(line, op, curtok, mnemonic, isrel))
		{
		}
		else if(curtok.type == IType::Label && !isrel)
		{
			if(Labels->count(*curtok.labelname) > 0)
			{
				op->immediate = (uint64_t) (*Labels)[*curtok.labelname] - instrptr;
				op->immpos = 4;	// special value
			}
		}
		else if(!isrel)
		{
			Error(StringUtil::Format("Expected label, immediate constant or register dereference after '->' operator"), linenum);
		}
		else
		{
			Error("Cannot perform relative jump to label", linenum);
		}
	}


	void ParseJumpToFunction(string& line, Operation* op, Token& curtok, string& mnemonic, bool isrel)
	{
		if((curtok.type == IType::Imm || curtok.type == IType::LBracket) && ParseJumpToAddress(line, op, curtok, mnemonic, isrel))
		{
			// nop
		}
		else if(curtok.type == IType::Function && !isrel)
		{
			if(Functions->count(*curtok.labelname))
			{
				op->immediate = (uint64_t) (*Functions)[*curtok.labelname] - instrptr;
				op->immpos = 4;	// special value

				// make sure we have an argument for number of args.
				Token t = GetNextToken(line);
				if(t.type == IType::Imm)
				{
					op->input1 = t.value;
				}
				else
				{
					Error("Expected number of arguments after 'call' instruction", linenum);
				}
			}
			else
			{
				Error(StringUtil::Format("Unrecognised function '%s'", curtok.labelname->c_str()), linenum);
			}
		}
		else if(!isrel)
		{
			Error(StringUtil::Format("Expected label, immediate constant or register dereference after '->' operator"), linenum);
		}
		else
		{
			Error("Cannot perform relative jump to label", linenum);
		}
	}

	bool CheckRelativeJump(string& line, Token& curtok, string& mnemonic)
	{
		bool isrel = false;
		if(curtok.type == IType::SizePrefix)
		{
			if(curtok.value != 0xFFFF)
				Error(StringUtil::Format("Instruction '%s' does not accept a size prefix, did you mean to use 'rel'?", mnemonic.c_str()), linenum);

			isrel = true;
			curtok = GetNextToken(line);
		}

		return isrel;
	}

	Operation& ParseLine(string line)
	{
		Operation* op = new Operation();
		memset(op, 0, sizeof(Operation));
		string mnemonic;

		for(int i = 0; i < line.length() && line[i] != ' '; i++)
			mnemonic += line[i];

		Instruction instr;
		try
		{
			instr = OpcodeMap->at(mnemonic);
		}
		catch(out_of_range& exception)
		{
			Error(StringUtil::Format("Unknown instruction '%s', aborting...", mnemonic.c_str()), linenum);
		}

		op->opcode = instr.opcode;

		// parse and setup the operation.
		// first check if we even require arguments.
		if(mnemonic.length() == line.length())
		{
			// single word instruction.
			// check if it needs arguments.
			if(ExpectsMoreArguments(instr, 0) || ExpectsMoreArguments(instr, 1) || ExpectsMoreArguments(instr, 2))
			{
				// we expect something.
				Error(StringUtil::Format("Instruction '%s' expects more than zero arguments", mnemonic.c_str()), linenum);
			}
			else
			{
				// setup the op and exit early.
				op->input1 = 0;
				op->input2 = 0;
				op->immediate = 0;
				op->output = 0;

				return *op;
			}
		}

		// else, check for the arguments.
		// can be a register, immediate, a -> register or a -> immediate.
		line = line.substr(mnemonic.length());
		Token tokens[3] = { { IType::None, 0xFFFFFFFFFFFFFFFF }, { IType::None, 0xFFFFFFFFFFFFFFFF }, { IType::None, 0xFFFFFFFFFFFFFFFF } };
		Token curtok;
		bool handledlabel = false;
		uint8_t sp = 0;

		// there's really no point doing it in a loop.
		// just scope it.
		do
		{
			// get the first token.
			curtok = GetNextToken(line);

			// the left arrow is syntactic sugar to make it look nicer.
			// if it's a left arrow, just skip it.
			if(curtok.type == IType::LeftArrow)
				curtok = GetNextToken(line);

			else if(curtok.type == IType::SizePrefix)
				sp = curtok.value;

			// handle these specially.
			// if we're a jump, expect an arrow, then a label.
			if(op->opcode == (*OpcodeMap)["jump"].opcode)
			{
				if(curpass > 0)
				{
					bool isrel = CheckRelativeJump(line, curtok, mnemonic);

					if(curtok.type != IType::Arrow)
						Error(StringUtil::Format("Expected '->' operator after '%s'", mnemonic.c_str()), linenum);

					ParseJumpToLabel(line, op, mnemonic, isrel);
				}
				break;
			}
			else if(op->opcode == (*OpcodeMap)["branch"].opcode)
			{
				if(curpass > 0)
				{
					bool isrel = CheckRelativeJump(line, curtok, mnemonic);

					if(curtok.type == IType::Reg || curtok.type == IType::Arrow)
					{
						op->input1 = curtok.type == IType::Reg ? curtok.value : ResultRegisterIndex;
						curtok = curtok.type == IType::Reg ? GetNextToken(line) : curtok;

						if(curtok.type != IType::Arrow)
							Error(StringUtil::Format("Expected '->' operator after '%s'", mnemonic.c_str()), linenum);
					}
					else if(curtok.type != IType::Arrow)
						Error(StringUtil::Format("Expected '->' operator after '%s'", mnemonic.c_str()), linenum);

					ParseJumpToLabel(line, op, mnemonic, isrel);
				}
				break;
			}
			else if(op->opcode == (*OpcodeMap)["call"].opcode)
			{
				// call
				if(curpass > 0)
				{
					bool isrel = CheckRelativeJump(line, curtok, mnemonic);
					ParseJumpToFunction(line, op, curtok, mnemonic, isrel);
				}
				break;
			}
			else if(op->opcode == (*OpcodeMap)["vmtrap"].opcode)
			{
				// handle vmtrap
				if(curpass > 0)
				{
				}
				break;
			}





			if(curtok.type == IType::SizePrefix)
				curtok = GetNextToken(line);


			if(curtok.type == IType::None && ExpectsMoreArguments(instr, 0))
				Error(StringUtil::Format("Instruction '%s' expects more than 0 arguments", mnemonic.c_str()), linenum);

			else if(CanAcceptMoreArguments(instr, 0) && curtok.type == IType::LBracket && IsValidType(instr.arg1, IType::Mem))
			{
				curtok = GetNextToken(line);
				assert(IsValidType(instr.arg1, IType::Mem));
				tokens[0] = curtok;
				tokens[0].deref = true;
				if((curtok = GetNextToken(line)).type != IType::RBracket)
					Error(StringUtil::Format("Expected ')' after operand"), linenum);
			}

			else if(CanAcceptMoreArguments(instr, 0) && IsValidType(instr.arg1, curtok.type))
				tokens[0] = curtok;

			else
				Error(StringUtil::Format("Type mismatch (expected type %d, got %d instead)", instr.arg1, curtok.type), linenum);


			// check that, if we're a memory reference instruction, that we have a size prefix.
			if(IsValidType(instr.arg1, IType::Mem) && !tokens[0].deref)
			{
				if(sp == 0)
					Error(StringUtil::Format("Instruction '%s' requires a size prefix for memory instructions.", mnemonic.c_str()), linenum);
			}








			curtok = GetNextToken(line);
			if(curtok.type == IType::SizePrefix)
			{
				if(sp != 0)
					Error(StringUtil::Format("More than one size specifier per instruction"), linenum);

				sp = curtok.value;
				curtok = GetNextToken(line);
			}

			if(curtok.type == IType::Arrow)
			{
				curtok = GetNextToken(line);
				if(curtok.type == IType::None)
					Error(StringUtil::Format("Expected token after '->' operator"), linenum);

				// check for mems.
				else if(CanAcceptMoreArguments(instr, 2) && curtok.type == IType::LBracket && IsValidType(instr.ret, IType::Mem))
				{
					curtok = GetNextToken(line);
					if(!IsValidType(instr.ret, IType::Mem))
						Error(StringUtil::Format("Instruction '%s' does not accept a memory operand", mnemonic.c_str()), linenum);

					tokens[2] = curtok;
					tokens[2].deref = true;

					if((curtok = GetNextToken(line)).type != IType::RBracket)
						Error(StringUtil::Format("Expected ')' after operand"), linenum);
				}

				else if(!IsValidType(instr.ret, curtok.type))
					Error(StringUtil::Format("Type mismatch (expected type %d, got %d instead)", instr.ret, curtok.type), linenum);

				else
					tokens[2] = curtok;

				break;
			}

			if(curtok.type == IType::None && ExpectsMoreArguments(instr, 1))
				Error(StringUtil::Format("Instruction '%s' expects more than 1 argument", mnemonic.c_str()), linenum);

			else if(CanAcceptMoreArguments(instr, 1) && IsValidType(instr.arg2, curtok.type))
				tokens[1] = curtok;

			else if(CanAcceptMoreArguments(instr, 1) && curtok.type == IType::LBracket && IsValidType(instr.arg2, IType::Mem))
			{
				curtok = GetNextToken(line);
				assert(IsValidType(instr.arg2, IType::Mem));
				tokens[1] = curtok;
				tokens[1].deref = true;
				if((curtok = GetNextToken(line)).type != IType::RBracket)
					Error(StringUtil::Format("Expected ')' after operand"), linenum);
			}

			else if(curtok.type != IType::None && curtok.type != IType::Invalid && (!CanAcceptMoreArguments(instr, 1) || !CanAcceptMoreArguments(instr, 2)))
				Error(StringUtil::Format("Too many arguments to instruction '%s', expected 1, got 2 (%d, %d)", mnemonic.c_str(), tokens[0].type, curtok.type), linenum);

			else if(!IsValidType(instr.arg2, curtok.type))
				Error(StringUtil::Format("Type mismatch (expected type %d, got %d instead)", instr.arg2, curtok.type), linenum);




			curtok = GetNextToken(line);
			if(curtok.type == IType::SizePrefix)
			{
				if(sp != 0)
					Error(StringUtil::Format("More than one size specifier per instruction"), linenum);

				sp = curtok.value;
				curtok = GetNextToken(line);
			}

			if(curtok.type == IType::None && CanAcceptMoreArguments(instr, 2) && (instr.ret == IType::OpReg || instr.ret == IType::OpReg || instr.ret == IType::Either))
			{
				curtok.value = ResultRegisterIndex;
				curtok.type = IType::Reg;
				tokens[2] = curtok;
				break;
			}

			if(!CanAcceptMoreArguments(instr, 2))
				break;


			if(curtok.type != IType::Arrow)
				Error(StringUtil::Format("Expected -> operator for return register"), linenum);


			// skip arrow
			curtok = GetNextToken(line);
			if(CanAcceptMoreArguments(instr, 2) && (instr.ret == IType::OpReg ? curtok.type == IType::Reg : curtok.type == IType::Imm))
				tokens[2] = curtok;

			else if(CanAcceptMoreArguments(instr, 2) && curtok.type == IType::LBracket && IsValidType(instr.ret, IType::Mem))
			{
				curtok = GetNextToken(line);
				assert(IsValidType(instr.ret, curtok.type));
				tokens[2] = curtok;
				tokens[2].deref = true;

				if((GetNextToken(line)).type != IType::RBracket)
					Error(StringUtil::Format("Expected ')' after operand"), linenum);
			}

			else
				Error(StringUtil::Format("Too many arguments to instruction '%s', expected 2, got 3", mnemonic.c_str()), linenum);





		} while(false);

		if(handledlabel)
			return *op;

		// now that we have all the things done, let's complete the instruction.

		// this theoretically means the size prefix (byte/double/quad/octo) can be anywhere in the instruction, doesn't matter.
		bool hasimm = false;
		op->deref_sizeprefix = 0;
		op->deref_sizeprefix |= (tokens[0].deref || tokens[1].deref || tokens[2].deref) ? 0x80 : 0;
		op->deref_sizeprefix |= sp;



		// fix up the data reference here. (halfway)
		switch(tokens[0].type)
		{
			case IType::Imm:
			case IType::OpImm:
				op->immediate = tokens[0].value;
				op->immpos = 1;
				hasimm = true;
				break;

			case IType::Reg:
			case IType::OpReg:
				op->input1 = tokens[0].value;
				break;

			case IType::DataReference:
				if(Data->find(*tokens[0].labelname) == Data->end())
					Error(StringUtil::Format("Unknown reference '%s'", tokens[0].labelname->c_str()), linenum);

				op->immediate = (*Data)[*tokens[0].labelname];
				op->immpos = 1;
				op->dataderef = true;
				break;

			default:
				break;
		}

		switch(tokens[1].type)
		{
			case IType::Imm:
			case IType::OpImm:
				if(hasimm)
					Error("Cannot have more than one immediate value in an instruction, aborting...", linenum);

				op->immediate = tokens[1].value;
				op->immpos = 2;
				break;

			case IType::Reg:
			case IType::OpReg:
				op->input2 = tokens[1].value;
				break;

			case IType::DataReference:
				if(Data->find(*tokens[1].labelname) == Data->end())
					Error(StringUtil::Format("Unknown reference '%s'", tokens[1].labelname->c_str()), linenum);

				op->immediate = (*Data)[*tokens[1].labelname];
				op->immpos = 2;
				op->dataderef = true;

			default:
				break;
		}

		switch(tokens[2].type)
		{
			case IType::Imm:
			case IType::OpImm:
				if(hasimm)
					Error("Cannot have more than one immediate value in an instruction, aborting...", linenum);

				op->immediate = tokens[2].value;
				op->immpos = 3;
				break;

			case IType::Reg:
			case IType::OpReg:
				op->output = tokens[2].value;
				break;

			case IType::DataReference:
				if(Data->find(*tokens[2].labelname) == Data->end())
					Error(StringUtil::Format("Unknown reference '%s'", tokens[2].labelname->c_str()), linenum);

				op->immediate = (*Data)[*tokens[2].labelname];
				op->immpos = 3;
				op->dataderef = true;

			default:
				break;
		}

		return *op;
	}













	bool ExpectsMoreArguments(Instruction& instr, int tok)
	{
		if(tok == 0)
		{
			if(instr.arg1 != IType::None && instr.arg1 != IType::OpReg && instr.arg1 != IType::OpImm)
				return true;
		}
		if(tok == 1)
		{
			if(instr.arg2 != IType::None && instr.arg2 != IType::OpReg && instr.arg2 != IType::OpImm)
				return true;
		}
		if(tok == 2)
		{
			if(instr.ret != IType::None && instr.ret != IType::OpReg && instr.ret != IType::OpImm)
				return true;
		}

		return false;
	}

	bool CanAcceptMoreArguments(Instruction& instr, int tok)
	{
		if(tok == 0)
		{
			if(instr.arg1 != IType::None)
				return true;
		}
		if(tok == 1)
		{
			if(instr.arg2 != IType::None)
				return true;
		}
		if(tok == 2)
		{
			if(instr.ret != IType::None)
				return true;
		}

		return false;
	}

	void Error(string reason, uint64_t line)
	{
		printf("Assembler error (line %lld): %s\n", line, reason.c_str());
		// fclose(read);
		exit(1);
	}

	void InitialiseOpcodeTable()
	{
		OpcodeMap = new map<string, Instruction>();

		(*OpcodeMap)["halt"]		= Instruction(0x00, IType::None, IType::None, IType::None);	// halts the cpu until interrupts.

		(*OpcodeMap)["load"]		= Instruction(0x01, IType::MemImm, IType::None, IType::Reg);	// loads a value from memory to a register.
		(*OpcodeMap)["store"]	= Instruction(0x02, IType::Reg, IType::None, IType::MemImm);	// stores a value from a register to memory.
		(*OpcodeMap)["copy"]		= Instruction(0x03, IType::Reg, IType::None, IType::Reg);		// copies a value from one register to another.

		(*OpcodeMap)["add"]		= Instruction(0x04, IType::Either, IType::Either, IType::OpReg);	// add the value in two registers.
		(*OpcodeMap)["subtract"]	= Instruction(0x05, IType::Either, IType::Either, IType::OpReg);	// subtract
		(*OpcodeMap)["multiply"]	= Instruction(0x06, IType::Either, IType::Either, IType::OpReg);	// multiply
		(*OpcodeMap)["divide"]	= Instruction(0x07, IType::Either, IType::Either, IType::OpReg);	// divide

		(*OpcodeMap)["cmpgt"]	= Instruction(0x08, IType::Either, IType::Either, IType::OpReg);	// greater than
		(*OpcodeMap)["cmplt"]	= Instruction(0x09, IType::Either, IType::Either, IType::OpReg);	// less than
		(*OpcodeMap)["cmpeq"]	= Instruction(0x0A, IType::Either, IType::Either, IType::OpReg);	// equals to
		(*OpcodeMap)["cmpgeq"]	= Instruction(0x0B, IType::Either, IType::Either, IType::OpReg);	// greater than or equals to
		(*OpcodeMap)["cmpleq"]	= Instruction(0x0C, IType::Either, IType::Either, IType::OpReg);	// less than or equals to

		(*OpcodeMap)["jump"]		= Instruction(0x0D, IType::None, IType::None, IType::None);	// unconditional jump to address
		(*OpcodeMap)["branch"]	= Instruction(0x0E, IType::OpReg, IType::None, IType::None);	// conditional jump depending on %rr or reg.

		(*OpcodeMap)["and"]		= Instruction(0x0F, IType::Either, IType::Either, IType::OpReg);	// bitwise and of two registers.
		(*OpcodeMap)["or"]		= Instruction(0x10, IType::Either, IType::Either, IType::OpReg);	// bitwise or
		(*OpcodeMap)["xor"]		= Instruction(0x11, IType::Either, IType::Either, IType::OpReg);	// exclusive or
		(*OpcodeMap)["not"]		= Instruction(0x12, IType::Either, IType::Either, IType::OpReg);	// not
		(*OpcodeMap)["negate"]	= Instruction(0x13, IType::Either, IType::None, IType::OpReg);	// negate a 2's complement signed integer.

		(*OpcodeMap)["increment"]	= Instruction(0x14, IType::Reg, IType::None, IType::None);	// Increment a register.
		(*OpcodeMap)["decrement"]	= Instruction(0x15, IType::Reg, IType::None, IType::None);	// Decrement a register.

		(*OpcodeMap)["move"]	= Instruction(0x16, IType::Imm, IType::None, IType::Reg); 	// stores an immediate value in a register.
		(*OpcodeMap)["call"]		= Instruction(0x17, IType::None, IType::None, IType::None); 	// calls a function.
		(*OpcodeMap)["return"]	= Instruction(0x18, IType::None, IType::None, IType::None); 	// returns from a function.
		(*OpcodeMap)["rcall"]		= Instruction(0x19, IType::None, IType::None, IType::None);	// does a call using runtime resolution of the name.
		(*OpcodeMap)["modulo"]	= Instruction(0x1A, IType::Reg, IType::Reg, IType::OpReg);	// returns the remainder of a division.

		(*OpcodeMap)["int"]		= Instruction(0x1B, IType::Either, IType::None, IType::None);	// does a CPU interrupt.
		(*OpcodeMap)["loadivt"]	= Instruction(0x1C, IType::MemImm, IType::None, IType::OpReg);	// load the interrupt table


		(*OpcodeMap)["vmtrap"]	= Instruction(0x1D, IType::None, IType::None, IType::None);	// do a 'system call' to the vm/hypervisor thing

		// register <=> stack bridging
		// stack opcodes have the MSB set (0x8xxx)
		(*OpcodeMap)["push"]		= Instruction(0x8000, IType::Either, IType::None, IType::None);	// pushes a value on stack. can accept either an imm or a reg.
		(*OpcodeMap)["pop"]		= Instruction(0x8001, IType::None, IType::None, IType::Reg);	// pops a value from stack, saving it in a register.



		// start of stack instructions
		(*OpcodeMap)["sadd"]		= Instruction(0x8002, IType::None, IType::None, IType::None);	// pops two values, adds them and pushes the result
		(*OpcodeMap)["ssubtract"]	= Instruction(0x8003, IType::None, IType::None, IType::None);	// subtracts
		(*OpcodeMap)["smultiply"]	= Instruction(0x8004, IType::None, IType::None, IType::None);	// multiplies
		(*OpcodeMap)["sdivide"]	= Instruction(0x8005, IType::None, IType::None, IType::None);	// divides
		(*OpcodeMap)["sinc"]		= Instruction(0x8006, IType::None, IType::None, IType::None);	// increments the value on top of the stack
		(*OpcodeMap)["sdec"]		= Instruction(0x8007, IType::None, IType::None, IType::None);	// decrements

		(*OpcodeMap)["scmpgt"]	= Instruction(0x8008, IType::None, IType::None, IType::None);	// pops top two values on stack, pushes result: greater than
		(*OpcodeMap)["scmplt"]	= Instruction(0x8009, IType::None, IType::None, IType::None);	// less than
		(*OpcodeMap)["scmpeq"]	= Instruction(0x800A, IType::None, IType::None, IType::None);	// equals to
		(*OpcodeMap)["scmpgeq"]	= Instruction(0x800B, IType::None, IType::None, IType::None);	// greater than or equals to
		(*OpcodeMap)["scmpleq"]	= Instruction(0x800C, IType::None, IType::None, IType::None);	// less than or equals to
	}
}











