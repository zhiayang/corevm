// Utilities.hpp
// Copyright (c) 2014, zhiayang@gmail.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

#pragma once
#include <string>
#include "/Library/Developer/CommandLineTools/usr/lib/clang/6.0/include/stdarg.h"

#define InstructionPointerIndex		124
#define ResultRegisterIndex		125
#define StackPointerIndex		126
#define FramePointerIndex		127

namespace Utilities
{
	std::string& LeftTrimString(std::string& s);
	std::string& RightTrimString(std::string& s);
	std::string& TrimString(std::string& s);


	void SleepMiliseconds(uint64_t milisec);
	void SleepNanoseconds(uint64_t nanosec);
	uint64_t Timestamp();
}

namespace StringUtil
{
	std::string& Format(const char* str, ...);
	std::string& ReplaceString(std::string& subject, const std::string& search, const std::string& replace);
}
