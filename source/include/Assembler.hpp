// Assembler.hpp
// Copyright (c) 2014, zhiayang@gmail.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

#pragma once
#include <stdint.h>
#include <stdio.h>
#include <fstream>
#include <vector>

namespace Assembler
{
	enum class IType
	{
		Invalid,			// 0
		Imm,			// 1
		Reg,			// 2
		OpImm,		// 3
		OpReg,			// 4
		None,			// 5
		Label,			// 6
		Function,		// 7
		Either,			// 8
		LBracket,		// 9
		RBracket,		// 10
		Mem,			// 11
		MemImm,		// 12
		MemReg,		// 13
		MemRegImm,		// 14
		SizePrefix,		// 15
		Arrow,			// 16
		LeftArrow,		// 17
		DataReference,		// 18
	};

	struct Operation
	{
		uint16_t opcode;
		uint64_t immediate;

		bool dataderef;
		uint8_t input1;
		uint8_t input2;
		uint8_t output;
		uint8_t immsign;
		uint8_t immpos;
		uint8_t deref_sizeprefix;

		bool operator==(const Operation& a)
		{
			return this->opcode == a.opcode &&
				this->immediate == a.immediate &&
				this->input1 == a.input1 &&
				this->input2 == a.input2 &&
				this->output == a.output &&
				this->immsign == a.immsign &&
				this->immpos == a.immpos &&
				this->deref_sizeprefix == a.deref_sizeprefix;
		}
	};

	struct Token
	{
		IType type;
		uint64_t value;
		bool deref;
		std::string* labelname;
	};

	struct Instruction
	{
		Instruction() : opcode(0), arg1(IType::None), arg2(IType::None), ret(IType::None) { }
		Instruction(uint16_t op, IType h1, IType h2, IType hr) : opcode(op), arg1(h1), arg2(h2), ret(hr) { }
		uint16_t opcode;

		IType arg1;
		IType arg2;
		IType ret;
	};

	struct DataBytes
	{
		uint8_t* raw;
		size_t length;
	};

	extern uint64_t linenum;

	void InitialiseOpcodeTable();
	std::vector<Operation>* Parse(FILE* f, uint64_t bytes, std::vector<DataBytes>* outdata);
	Operation& ParseLine(std::string line);
	Token& GetNextToken(std::string& line);
	bool ExpectsMoreArguments(Instruction& instr, int tok);
	bool CanAcceptMoreArguments(Instruction& instr, int tok);

	std::string ParseWord(std::string& line, char stop = ' ', bool newlineokay = false);
	void Error(std::string reason, uint64_t line);


	void Serialise(std::vector<Operation>* ops, std::string filename, std::vector<DataBytes>* outdata);
}




