// VM.hpp
// Copyright (c) 2014, zhiayang@gmail.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

#pragma once
#include <stdint.h>
#include <cstdio>
#include <map>
#include <list>
#include <vector>
#include <functional>
#include <thread>

namespace VM
{
	class CPU;

	namespace Config
	{
		void Initialise(CPU* cpu);
		uint64_t GetCyclesPerSecond();
		uint64_t GetMemorySize();
		uint64_t GetMemoryControllers();

		std::string* GetRootFS();

		uint64_t GetResolutionX();
		uint64_t GetResolutionY();
	}

	namespace Debug
	{
		void Initialise(CPU* cpu);
	}

	struct Instruction
	{
		uint16_t opcode;

		uint8_t input1;
		uint8_t input2;
		uint8_t output;

		uint8_t padding1;
		uint8_t immpos;
		uint8_t derefop;

		uint64_t immediate;

	} __attribute__ ((packed));


	class InstructionPipeline
	{
		friend class CPU;

		InstructionPipeline(CPU* cp);
		Instruction* Fetch();
		void Prefetch(uint64_t instructions);

		CPU* cpu;
	};

	class Decoder
	{
		friend class CPU;

		Decoder(CPU* cpu);
		void DecodeAndExecute(Instruction* instr);

		void SetResult(uint64_t reg, uint64_t val);
		uint64_t GetOperand(Instruction* instr, uint64_t op);


		void Load(Instruction* instr);
		void Store(Instruction* instr);
		void Copy(Instruction* instr);

		void GetArithmeticOps(Instruction* instr, uint64_t& o1, uint64_t& o2);
		void Add(Instruction* instr);
		void Subtract(Instruction* instr);
		void Multiply(Instruction* instr);
		void Divide(Instruction* instr);

		void GreaterThan(Instruction* instr);
		void LessThan(Instruction* instr);
		void EqualsTo(Instruction* instr);
		void GreaterThanOrEqualsTo(Instruction* instr);
		void LessThanOrEqualsTo(Instruction* instr);

		void Jump(Instruction* instr);
		void Branch(Instruction* instr);

		void BitwiseAnd(Instruction* instr);
		void BitwiseOr(Instruction* instr);
		void BitwiseXor(Instruction* instr);
		void BitwiseNot(Instruction* instr);
		void Negate(Instruction* instr);

		void Increment(Instruction* instr);
		void Decrement(Instruction* instr);

		void Move(Instruction* instr);
		void Call(Instruction* instr);
		void Return(Instruction* instr);
		void RuntimeCall(Instruction* instr);
		void Modulo(Instruction* instr);

		// stack things
		void Push(Instruction* instr);
		void Pop(Instruction* instr);

		void Stack_Add(Instruction* instr);
		void Stack_Subtract(Instruction* instr);
		void Stack_Multiply(Instruction* instr);
		void Stack_Divide(Instruction* instr);
		void Stack_Increment(Instruction* instr);
		void Stack_Decrement(Instruction* instr);

		void Stack_GreaterThan(Instruction* instr);
		void Stack_LessThan(Instruction* instr);
		void Stack_EqualsTo(Instruction* instr);
		void Stack_GreaterThanOrEqualsTo(Instruction* instr);
		void Stack_LessThanOrEqualsTo(Instruction* instr);





		CPU* cpu;

	private:
		void Push(uint64_t val);
		uint64_t Pop();

	};


	class MemoryController
	{
		public:
			MemoryController(uint64_t domainstart, uint64_t domlength, uint64_t latency);
			bool WithinDomain(uint64_t addr);

			uint8_t* GetRawPointer(uint64_t addr = 0);

			uint8_t LoadByte(uint64_t addr);
			uint16_t Load16(uint64_t addr);
			uint32_t Load32(uint64_t addr);
			uint64_t Load64(uint64_t addr);

			void StoreByte(uint64_t addr, uint8_t val);
			void Store16(uint64_t addr, uint16_t val);
			void Store32(uint64_t addr, uint32_t val);
			void Store64(uint64_t addr, uint64_t val);

			uint8_t* LoadBytes(uint64_t addr, uint64_t length);
			uint16_t* Load16(uint64_t addr, uint64_t length);
			uint32_t* Load32(uint64_t addr, uint64_t length);
			uint64_t* Load64(uint64_t addr, uint64_t length);

			void StoreBytes(uint64_t addr, uint8_t* bytes, uint64_t length);
			void Store16(uint64_t addr, uint16_t* buf, uint64_t length);
			void Store32(uint64_t addr, uint32_t* buf, uint64_t length);
			void Store64(uint64_t addr, uint64_t* buf, uint64_t length);

			uint64_t domstart;
			uint64_t domlength;
			uint64_t latency;

		private:
			uint8_t* backingstore;
	};

	class CacheController
	{
		public:
			CacheController(uint64_t latency);
			bool IsCached(uint64_t addr);

			uint8_t LoadByte(uint64_t addr);
			uint16_t Load16(uint64_t addr);
			uint32_t Load32(uint64_t addr);
			uint64_t Load64(uint64_t addr);

			void StoreByte(uint64_t addr, uint8_t val);
			void Store16(uint64_t addr, uint16_t val);
			void Store32(uint64_t addr, uint32_t val);
			void Store64(uint64_t addr, uint64_t val);


			uint8_t* LoadByte(uint64_t addr, uint64_t length);
			uint16_t* Load16(uint64_t addr, uint64_t length);
			uint32_t* Load32(uint64_t addr, uint64_t length);
			uint64_t* Load(uint64_t addr, uint64_t length);

			void CacheByte(uint64_t addr, uint8_t* bytes, uint64_t length);
			void Cache16(uint64_t addr, uint16_t* buf, uint64_t length);
			void Cache32(uint64_t addr, uint32_t* buf, uint64_t length);
			void Cache(uint64_t addr, uint64_t* buf, uint64_t length);

			// always in bytes
			void Purge(uint64_t addr, uint64_t length);
			void Clear();

		private:
			uint8_t* backingstore;
	};


	class MemoryManager
	{
		template< typename T >
		class range
		{
			public:
				range(T const & center) : min_(center), max_(center) { }
				range(T const & min, T const & max) : min_(min), max_(max) { }

				T min() const { return min_; }
				T max() const { return max_; }

			private:
				T min_;
				T max_;
		};

		// Detection of outside of range to the left (smaller values):
		//
		// a range lhs is left (smaller) of another range if both lhs.min() and lhs.max()
		// are smaller than rhs.min().

		template <typename T>
		struct left_of_range : public std::binary_function<range<T>, range<T>, bool>
		{
			bool operator()(range<T> const & lhs, range<T> const & rhs) const
			{
				return lhs.min() < rhs.min() && lhs.max() <= rhs.min();
			}
		};



		public:
			MemoryManager(uint64_t size, std::vector<MemoryController*> controllers, std::vector<CacheController*> cachelist);
			MemoryController* GetController(uint64_t addr);

		private:
			std::map<range<uint64_t>, MemoryController*, left_of_range<uint64_t> >* memcontrollers;
			std::vector<CacheController*>* caches;
			uint64_t memsize;
	};


	class Filesystem
	{
		public:
			Filesystem(std::string rootdir);
			uint8_t* ReadFile(std::string path, uint64_t& size);

		private:
			std::string* rootfs;
	};

	class CPU
	{
		friend class Decoder;

		public:
			CPU(Filesystem* fs);
			void Start();
			void Stop();

			uint64_t* GetRegisters();
			uint64_t GetResultRegister();
			uint64_t GetInstructionPointer();
			MemoryManager* GetMemoryManager();
			uint64_t GetFramePointer();
			uint64_t GetStackPointer();

			void SetResultRegister(uint64_t r);
			void SetInstructionPointer(uint64_t r);
			void SetFramePointer(uint64_t f);

			void AdvanceInstructionPointer(Instruction* instr);

			static CPU* Bootstrap(std::string path);

		// private:
			void CPULoop();
			bool stop;
			uint64_t* Registers;

			std::thread* debugthread;
			InstructionPipeline* pipeline;
			Decoder* decoder;
			Filesystem* filesystem;
			MemoryManager* memmanager;
	};
}
