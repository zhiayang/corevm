// VxEF.hpp
// Copyright (c) 2014 - The Foreseeable Future, zhiayang@gmail.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported


#include <stdint.h>

struct CoreHeader
{
	uint8_t Signature[4];			// 'V', 'x', 'E', 'F'.
	uint8_t padding[4];
	uint64_t EntryPoint;			// Entry point of the program: value is ExecutableSize + actualoffset

	uint64_t ExecutableSize;		// size in bytes of the executable section.
	uint64_t ExecutableLoadAddr;		// address to load the executable section to, during program start.

	uint64_t DataSize;			// size in bytes of the data section.
	uint64_t DataLoadAddr;		// same as above, except for data.

	// executable section starts directly after the header, and the data section directly after that.
	// pad as required to keep to 8 byte alignment.

} __attribute__ ((packed));





