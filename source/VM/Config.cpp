// Config.cpp
// Copyright (c) 2014, zhiayang@gmail.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

#include <stdint.h>
#include "VM.hpp"
#include <string>
#include <iostream>
#include <unordered_map>

#define RAPIDXML_NO_EXCEPTIONS
#include <../../libraries/rapidxml.hpp>

namespace rapidxml
{
	void parse_error_handler(char const* a, void* b)
	{
		abort();
	}
}

using namespace std;
using namespace rapidxml;

namespace VM {
namespace Config
{
	static uint64_t tmp_cachesizes[] = {	 0x2000, 0x10000 };
	static std::unordered_map<string, string>* config;
	static CPU* cpu;

	void Initialise(CPU* thecpu)
	{
		cpu = thecpu;
		config = new unordered_map<string, string>();

		xml_document<> document;

		uint64_t size = 0;
		uint8_t* buf = cpu->filesystem->ReadFile("/System/Preferences/config.plist", size);
		string data = string((char*) buf);
		delete[] buf;

		data = data.substr(0, size);
		document.parse<0>((char*) data.c_str());

		// make sure we're reading a plist.
		assert(strcmp(document.first_node()->name(), "plist") == 0);
		xml_node<>* plist = document.first_node("plist");

		// read the dict entry.
		xml_node<>* dict = plist->first_node("dict");

		for(xml_node<>* entry = dict->first_node(); entry; entry = entry->next_sibling())
		{
			if(!entry->next_sibling())
			{
				printf("Key %s missing value, ignoring...\n", entry->value());
				continue;
			}

			if(strcmp(entry->name(), "key") != 0)
			{
				printf("Malformed plist, expected <key>...</key> but got <%s> instead\n", entry->name());
				continue;
			}


			string keyval = entry->value();
			string valval = entry->next_sibling()->value();

			// check if we got a boolean.
			if(valval.length() == 0)
			{
				string valname = entry->next_sibling()->name();
				if(valname == string("true") || valname == string("false"))
					valval = valname;

				else
				{
					printf("Malformed plist, expected boolean with either <true/> or <false/> but got <%s> instead\n", valname.c_str());
					continue;
				}
			}

			(*config)[string(entry->value())] =  string(entry->next_sibling()->value());
			entry = entry->next_sibling();
		}
	}

	uint64_t GetCyclesPerSecond()
	{
		return strtoull((*config)["CyclesPerSecond"].c_str(), nullptr, 0);
	}

	uint64_t GetMemorySize()
	{
		return strtoull((*config)["MemorySize"].c_str(), nullptr, 0);
	}

	uint64_t GetMemoryControllers()
	{
		return 2;
	}

	uint64_t GetCacheLevels()
	{
		// L1 cache, L2 cache
		return 2;
	}

	uint64_t GetCacheSize(uint64_t level)
	{
		return tmp_cachesizes[level];
	}

	std::string* GetRootFS()
	{
		return new std::string("rootfs");
	}

	uint64_t GetResolutionX()
	{
		return 1024;
	}

	uint64_t GetResolutionY()
	{
		return 600;
	}
}
}










