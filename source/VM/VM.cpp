// VM.cpp
// Copyright (c) 2014, zhiayang@gmail.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

#include <cstdio>
#include <iostream>
#include <sys/stat.h>
#include <thread>
#include "VM.hpp"
#include "Utilities.hpp"


using namespace std;

int main(int argc, char** argv)
{
	// start the VM CPU on a separate thread
	std::thread cpu = std::thread(VM::CPU::Bootstrap, ".");
	cpu.join();

	return 0;
}
