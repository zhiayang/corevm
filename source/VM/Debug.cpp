// Debug.cpp
// Copyright (c) 2014, zhiayang@gmail.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

#include <stdint.h>
#include <string>
#include <iostream>

#include "VM.hpp"
#include "Utilities.hpp"

#include <ncurses.h>

#define DEBUG 1

namespace VM {
namespace Debug
{
	CPU* TheCPU;
	WINDOW* TheWindow;

	void PrintRegisters()
	{
		while(getch() != 'q')
		{
			if(DEBUG)
			{
				clear();
				uint64_t* ptr = TheCPU->GetRegisters();
				printw("Control Registers:\n");
				printw("ip: 0x%.16llx\t\tsp: 0x%.16llx\n", ptr[InstructionPointerIndex], ptr[StackPointerIndex]);
				printw("rr: 0x%.16llx\n\n", ptr[ResultRegisterIndex]);

				for(int i = 1; i <= 32; i += 2)
				{
					printw("r%02d: 0x%.16llx\t\tr%02d: 0x%.16llx\n", i, ptr[i], i + 1, ptr[i + 1]);
				}

				refresh();
			}
		}
	}

	void Initialise(CPU* cpu)
	{
		TheCPU = cpu;

		TheWindow = initscr();
		cbreak();
		nodelay(TheWindow, true);

		PrintRegisters();

		endwin();

		TheCPU->stop = true;
	}
}
}










