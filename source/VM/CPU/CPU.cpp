// CPU.cpp
// Copyright (c) 2014, zhiayang@gmail.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

#include <stdint.h>
#include <sys/time.h>
#include <cstring>
#include <thread>
#include <iostream>
#include <cassert>

#include <ncurses.h>

#include "VM.hpp"
#include "Utilities.hpp"
#include "CoreExecFormat.hpp"

namespace VM
{
	static void LoadROM(CPU* cpu)
	{
		uint64_t size = 0;
		uint8_t* rom = cpu->filesystem->ReadFile("/boot/rom", size);

		// parse the rom file.
		CoreHeader* header = (CoreHeader*) rom;
		assert(header->Signature[0] == 'c');
		assert(header->Signature[1] == 'o');
		assert(header->Signature[2] == 'r');
		assert(header->Signature[3] == 'e');


		// load the executable section.
		uint8_t* execptr = rom + sizeof(CoreHeader);
		uint8_t* dataptr = rom + header->ExecutableSize;

		cpu->GetMemoryManager()->GetController(header->ExecutableLoadAddr)->StoreBytes(header->ExecutableLoadAddr, execptr, header->ExecutableSize);
		cpu->GetMemoryManager()->GetController(header->DataLoadAddr)->StoreBytes(header->DataLoadAddr, dataptr, header->DataSize);

		cpu->SetInstructionPointer(header->EntryPoint);
	}

	CPU* CPU::Bootstrap(std::string path)
	{
		Filesystem* fs = new Filesystem(path + "/" + *Config::GetRootFS());
		CPU* TheCPU = new CPU(fs);
		LoadROM(TheCPU);

		// start debug thread
		TheCPU->debugthread = new std::thread(Debug::Initialise, TheCPU);

		// set the stack pointer to 1MB.
		TheCPU->Registers[StackPointerIndex] = 0x00100000;


		TheCPU->Start();

		// not reached.
		return TheCPU;
	}

	CPU::CPU(Filesystem* fs)
	{
		// init CPU subsystems
		this->pipeline = new InstructionPipeline(this);
		this->decoder = new Decoder(this);
		this->Registers = new uint64_t[128]();
		this->filesystem = fs;
		this->stop = false;

		Config::Initialise(this);

		std::vector<MemoryController*> controllers;

		// split memory into two by default.
		uint64_t acc = 0;
		uint64_t dom = Config::GetMemorySize() / Config::GetMemoryControllers();
		for(uint64_t i = 0; i < Config::GetMemoryControllers(); i++)
		{
			controllers.push_back(new MemoryController(acc, dom, 0));
			acc += dom;
		}

		this->memmanager = new MemoryManager(Config::GetMemorySize(), controllers, std::vector<CacheController*>());
		memset(this->Registers, 0, sizeof(uint64_t) * 128);
	}

	void CPU::Start()
	{
		this->CPULoop();
	}

	void CPU::Stop()
	{
		// responsible for cleanup.
		this->debugthread->join();
		exit(0);
	}

	void CPU::CPULoop()
	{
		uint64_t cps = Config::GetCyclesPerSecond();
		while(!this->stop)
		{
			uint64_t a = Utilities::Timestamp();

			Instruction* instr = this->pipeline->Fetch();
			this->decoder->DecodeAndExecute(instr);

			// timestamp doesn't exactly return the actual timestamp, but nobody gives a shit.
			Utilities::SleepNanoseconds(((1000 * 1000 * 1000) / cps) - (Utilities::Timestamp() - a));
		}

		if(this->stop)
			this->Stop();
	}

	void CPU::AdvanceInstructionPointer(Instruction* instr)
	{
		this->Registers[InstructionPointerIndex] += sizeof(Instruction) - (instr->immpos > 0 ? 0 : sizeof(uint64_t));
	}

	uint64_t CPU::GetInstructionPointer()
	{
		return this->Registers[InstructionPointerIndex];
	}

	uint64_t CPU::GetFramePointer()
	{
		return this->Registers[FramePointerIndex];
	}

	uint64_t CPU::GetStackPointer()
	{
		return this->Registers[StackPointerIndex];
	}

	uint64_t* CPU::GetRegisters()
	{
		return this->Registers;
	}

	void CPU::SetResultRegister(uint64_t r)
	{
		this->Registers[ResultRegisterIndex] = r;
	}

	void CPU::SetInstructionPointer(uint64_t r)
	{
		this->Registers[InstructionPointerIndex] = r;
	}

	void CPU::SetFramePointer(uint64_t f)
	{
		this->Registers[FramePointerIndex] = f;
	}

	MemoryManager* CPU::GetMemoryManager()
	{
		return this->memmanager;
	}
}




