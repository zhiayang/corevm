// Decoder.cpp
// Copyright (c) 2014, zhiayang@gmail.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

#include <stdint.h>
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>

#include "VM.hpp"
#include "Utilities.hpp"
#include <ncurses.h>

#define IsValidGPR(x)		(x > 0 && x <= 32)
#define IsValidReg(x)		(x > 0 && x <= 128)
#define IsDerefOp(x)		(x & 0x80)
#define tempfail()		do{ clear(); refresh(); endwin(); assert(0); } while(0);
#define WordSize		8


namespace VM
{
	// if 'reg' is a valid register index, set it to val.
	// if not, use the result register.
	void Decoder::SetResult(uint64_t reg, uint64_t val)
	{
		if(IsValidReg(reg))
			this->cpu->Registers[reg] = val;

		else
			this->cpu->Registers[ResultRegisterIndex] = val;
	}


	Decoder::Decoder(CPU* thecpu)
	{
		this->cpu = thecpu;
	}

	void Decoder::DecodeAndExecute(Instruction* instr)
	{
		// advance the instruction pointer first.
		// this is so we can (hopefully?) prefetch the next X instructions
		this->cpu->AdvanceInstructionPointer(instr);
		switch(instr->opcode)
		{
			case 0x00:	// halt
			case 0x01:	// load
				this->Load(instr);
				break;

			case 0x02:	// store
				this->Store(instr);
				break;

			case 0x03:	// copy
				this->Copy(instr);
				break;

			case 0x04:	// add
				this->Add(instr);
				break;

			case 0x05:	// sub
				this->Subtract(instr);
				break;

			case 0x06:	// mul
				this->Multiply(instr);
				break;

			case 0x07:	// div
				this->Divide(instr);
				break;

			case 0x08:	// greater
				this->GreaterThan(instr);
				break;

			case 0x09:	// less
				this->LessThan(instr);
				break;

			case 0x0A:	// equal
				this->EqualsTo(instr);
				break;

			case 0x0B:	// greater or equal
				this->GreaterThanOrEqualsTo(instr);
				break;

			case 0x0C:	// less or equal
				this->LessThanOrEqualsTo(instr);
				break;

			case 0x0D:	// jump
				this->Jump(instr);
				break;

			case 0x0E:	// branch
				this->Branch(instr);
				break;

			case 0x0F:	// and
				this->BitwiseAnd(instr);
				break;

			case 0x10:	// or
				this->BitwiseOr(instr);
				break;

			case 0x11:	// xor
				this->BitwiseOr(instr);
				break;

			case 0x12:	// not
				this->BitwiseNot(instr);
				break;

			case 0x13:	// negate
				this->Negate(instr);
				break;

			case 0x14:	// inc
				this->Increment(instr);
				break;

			case 0x15:	// dec
				this->Decrement(instr);
				break;

			case 0x16:	// mov
				this->Move(instr);
				break;

			case 0x17:
				this->Call(instr);
				break;

			case 0x18:
				this->Return(instr);
				break;

			case 0x19:
				this->RuntimeCall(instr);
				break;

			case 0x1A:
				this->Modulo(instr);
				break;

			case 0x8000:
				this->Push(instr);
				break;

			case 0x8001:
				this->Pop(instr);
				break;

			case 0x8002:
				this->Stack_Add(instr);
				break;

			case 0x8003:
				this->Stack_Subtract(instr);
				break;

			case 0x8004:
				this->Stack_Multiply(instr);
				break;

			case 0x8005:
				this->Stack_Divide(instr);
				break;

			case 0x8006:
				this->Stack_Increment(instr);
				break;

			case 0x8007:
				this->Stack_Decrement(instr);
				break;

			case 0x8008:
				this->Stack_GreaterThan(instr);
				break;

			case 0x8009:
				this->Stack_LessThan(instr);
				break;

			case 0x800A:
				this->Stack_EqualsTo(instr);
				break;

			case 0x800B:
				this->Stack_GreaterThanOrEqualsTo(instr);
				break;

			case 0x800C:
				this->Stack_LessThanOrEqualsTo(instr);
				break;

			default:
				tempfail();
		}
	}

	uint64_t Decoder::GetOperand(Instruction* instr, uint64_t op)
	{
		if(op == 1)
		{
			if(IsValidReg(instr->input1) && instr->immpos != op)
				return this->cpu->Registers[instr->input1];

			else if(instr->immpos == op)
				return instr->immediate;

			else
				tempfail();
		}
		else if(op == 2)
		{
			if(IsValidReg(instr->input2) && instr->immpos != op)
				return this->cpu->Registers[instr->input2];

			else if(instr->immpos == op)
				return instr->immediate;

			else
				tempfail();
		}
		else if(op == 3)
		{
			if(IsValidReg(instr->output) && instr->immpos != op)
				return this->cpu->Registers[instr->output];

			else if(instr->immpos == op)
				return instr->immediate;

			else
				tempfail();
		}
		else
			tempfail();
	}

	// internal helper function
	void Decoder::GetArithmeticOps(Instruction* instr, uint64_t& o1, uint64_t& o2)
	{
		o1 = this->GetOperand(instr, 1);
		o2 = this->GetOperand(instr, 2);
	}


	void Decoder::Load(Instruction* instr)
	{
		if(IsValidReg(instr->output))
		{
			uint64_t val = 0;
			uint8_t sz = instr->derefop & 0x7F;

			// check if we're a deref op.
			if(IsDerefOp(instr->derefop))
			{
				if(IsValidReg(instr->input1))
				{
					uint64_t ptr = this->GetOperand(instr, 1);

					if(sz == 1)	val = this->cpu->GetMemoryManager()->GetController(ptr)->LoadByte(ptr);
					else if(sz == 2)	val = this->cpu->GetMemoryManager()->GetController(ptr)->Load16(ptr);
					else if(sz == 4)	val = this->cpu->GetMemoryManager()->GetController(ptr)->Load32(ptr);
					else if(sz == 8)	val = this->cpu->GetMemoryManager()->GetController(ptr)->Load64(ptr);
					else		tempfail();
				}
				else
					tempfail();
			}
			else if(instr->immpos == 1)
			{
				uint64_t ptr = instr->immediate;

				if(sz == 1)	val = this->cpu->GetMemoryManager()->GetController(ptr)->LoadByte(ptr);
				else if(sz == 2)	val = this->cpu->GetMemoryManager()->GetController(ptr)->Load16(ptr);
				else if(sz == 4)	val = this->cpu->GetMemoryManager()->GetController(ptr)->Load32(ptr);
				else if(sz == 8)	val = this->cpu->GetMemoryManager()->GetController(ptr)->Load64(ptr);
				else		tempfail();
			}
			else
				tempfail();

			this->SetResult(instr->output, val);
		}
		else
			tempfail();
	}

	void Decoder::Store(Instruction* instr)
	{
		if(IsValidReg(instr->input1))
		{
			uint64_t o1 = this->GetOperand(instr, 1);
			uint8_t sz = instr->derefop & 0x7F;

			// check if we're a deref op (ie. moving to pointer in register)
			if(IsDerefOp(instr->derefop))
			{
				// we are. make sure the target register is valid.
				if(IsValidReg(instr->output))
				{
					// get the value.
					uint64_t ptr = this->GetOperand(instr, 3);

					if(sz == 1)	this->cpu->GetMemoryManager()->GetController(ptr)->StoreByte(ptr, o1 & 0xFF);
					else if(sz == 2)	this->cpu->GetMemoryManager()->GetController(ptr)->Store16(ptr, o1 & 0xFFFF);
					else if(sz == 4)	this->cpu->GetMemoryManager()->GetController(ptr)->Store32(ptr, o1 & 0xFFFFFFFF);
					else if(sz == 8)	this->cpu->GetMemoryManager()->GetController(ptr)->Store64(ptr, o1);
					else		tempfail();
				}
				else
					tempfail();
			}
			else if(instr->immpos == 3)
			{
				uint64_t ptr = instr->immediate;

				if(sz == 1)	this->cpu->GetMemoryManager()->GetController(ptr)->StoreByte(ptr, o1 & 0xFF);
				else if(sz == 2)	this->cpu->GetMemoryManager()->GetController(ptr)->Store16(ptr, o1 & 0xFFFF);
				else if(sz == 4)	this->cpu->GetMemoryManager()->GetController(ptr)->Store32(ptr, o1 & 0xFFFFFFFF);
				else if(sz == 8)	this->cpu->GetMemoryManager()->GetController(ptr)->Store64(ptr, o1);
				else		tempfail();
			}
			else
				tempfail();
		}
		else
			tempfail();
	}

	void Decoder::Copy(Instruction* instr)
	{
		// copy a -> b.
		if(IsValidReg(instr->input1) && IsValidReg(instr->output))
		{
			this->cpu->Registers[instr->output] = this->cpu->Registers[instr->input1];
		}
	}

	void Decoder::Add(Instruction* instr)
	{
		uint64_t o1 = 0;
		uint64_t o2 = 0;

		this->GetArithmeticOps(instr, o1, o2);
		this->SetResult(instr->output, o1 + o2);
	}

	void Decoder::Subtract(Instruction* instr)
	{
		uint64_t o1 = 0;
		uint64_t o2 = 0;

		this->GetArithmeticOps(instr, o1, o2);
		this->SetResult(instr->output, o1 - o2);
	}

	void Decoder::Multiply(Instruction* instr)
	{
		uint64_t o1 = 0;
		uint64_t o2 = 0;

		this->GetArithmeticOps(instr, o1, o2);
		this->SetResult(instr->output, o1 * o2);
	}

	void Decoder::Divide(Instruction* instr)
	{
		uint64_t o1 = 0;
		uint64_t o2 = 0;

		this->GetArithmeticOps(instr, o1, o2);
		if(o2 == 0)
			tempfail();

		this->SetResult(instr->output, o1 / o2);
	}

	void Decoder::Modulo(Instruction* instr)
	{
		uint64_t o1 = this->GetOperand(instr, 1);
		uint64_t o2 = this->GetOperand(instr, 2);

		this->SetResult(this->GetOperand(instr, 3), o1 % o2);
	}

	void Decoder::GreaterThan(Instruction* instr)
	{
		uint64_t o1 = this->GetOperand(instr, 1);
		uint64_t o2 = this->GetOperand(instr, 2);

		this->SetResult(this->GetOperand(instr, 3), o1 > o2);
	}
	void Decoder::LessThan(Instruction* instr)
	{
		uint64_t o1 = this->GetOperand(instr, 1);
		uint64_t o2 = this->GetOperand(instr, 2);

		this->SetResult(this->GetOperand(instr, 3), o1 < o2);
	}
	void Decoder::EqualsTo(Instruction* instr)
	{
		uint64_t o1 = this->GetOperand(instr, 1);
		uint64_t o2 = this->GetOperand(instr, 2);

		this->SetResult(this->GetOperand(instr, 3), o1 == o2);
	}
	void Decoder::GreaterThanOrEqualsTo(Instruction* instr)
	{
		uint64_t o1 = this->GetOperand(instr, 1);
		uint64_t o2 = this->GetOperand(instr, 2);

		this->SetResult(this->GetOperand(instr, 3), o1 >= o2);
	}
	void Decoder::LessThanOrEqualsTo(Instruction* instr)
	{
		uint64_t o1 = this->GetOperand(instr, 1);
		uint64_t o2 = this->GetOperand(instr, 2);

		this->SetResult(this->GetOperand(instr, 3), o1 <= o2);
	}

	void Decoder::Jump(Instruction* instr)
	{
		if(instr->immpos == 4)
		{
			// this is a relative jump
			int64_t offset = (int64_t) instr->immediate;
			this->cpu->Registers[InstructionPointerIndex] += (offset - (16 - (instr->immpos > 0 ? 0 : 8)));
		}
		else if(instr->immpos == 1)
		{
			// absolute jump.
			this->cpu->Registers[InstructionPointerIndex] = instr->immediate;
		}
		else if(instr->derefop & 0x80 && IsValidReg(instr->input2))
		{
			// jump by dereferencing register.
			// if we're a rel, immpos will be 4.
			if(instr->immpos == 4)
			{
				// rel jump.
				int64_t offset = this->cpu->Registers[instr->input2];
				this->cpu->Registers[InstructionPointerIndex] += (offset - (16 - (instr->immpos > 0 ? 0 : 8)));
			}
			else if(instr->immpos == 0)
			{
				this->cpu->Registers[InstructionPointerIndex] = this->cpu->Registers[instr->input2];
			}
			else
				tempfail();
		}
		else
			tempfail();
	}

	void Decoder::Branch(Instruction* instr)
	{
		if(IsValidReg(instr->input1))
		{
			if(this->cpu->Registers[instr->input1])
				this->Jump(instr);
		}
		else
			tempfail();
	}

	void Decoder::BitwiseAnd(Instruction* instr)
	{
		uint64_t o1 = this->GetOperand(instr, 1);
		uint64_t o2 = this->GetOperand(instr, 2);

		this->SetResult(this->GetOperand(instr, 3), o1 & o2);
	}

	void Decoder::BitwiseOr(Instruction* instr)
	{
		uint64_t o1 = this->GetOperand(instr, 1);
		uint64_t o2 = this->GetOperand(instr, 2);

		this->SetResult(this->GetOperand(instr, 3), o1 | o2);
	}
	void Decoder::BitwiseXor(Instruction* instr)
	{
		uint64_t o1 = this->GetOperand(instr, 1);
		uint64_t o2 = this->GetOperand(instr, 2);

		this->SetResult(this->GetOperand(instr, 3), o1 ^ o2);
	}
	void Decoder::BitwiseNot(Instruction* instr)
	{
		uint64_t o1 = this->GetOperand(instr, 1);
		this->SetResult(this->GetOperand(instr, 3), ~o1);
	}

	void Decoder::Negate(Instruction* instr)
	{
		// todo: have to properly handle signed things first.
	}

	void Decoder::Increment(Instruction* instr)
	{
		if(IsValidReg(instr->input1))
			this->cpu->Registers[instr->input1]++;
	}
	void Decoder::Decrement(Instruction* instr)
	{
		if(IsValidReg(instr->input1))
			this->cpu->Registers[instr->input1]--;
	}

	void Decoder::Move(Instruction* instr)
	{
		// store an immediate value in a register.
		if(IsValidReg(instr->output))
		{
			this->cpu->Registers[instr->output] = instr->immediate;
		}
		else
			tempfail();
	}
	void Decoder::Call(Instruction* instr)
	{
		// because we already advanced the instruction pointer,
		// our return address is simply the current IP.

		// push things.
		// numargs stored in 'input1' by the assembler.
		this->Push(instr->input1);
		this->Push(this->cpu->GetInstructionPointer());
		this->Push(this->cpu->GetFramePointer());
		this->cpu->SetFramePointer(this->cpu->GetStackPointer());


		// move instruction pointer to the function's.
		int64_t offset = (int64_t) instr->immediate;
		this->cpu->Registers[InstructionPointerIndex] += (offset - (16 - (instr->immpos > 0 ? 0 : 8)));
	}
	void Decoder::Return(Instruction* instr)
	{
		// move stack pointer back to frame pointer.
		this->cpu->Registers[StackPointerIndex] = this->cpu->GetFramePointer();
		this->cpu->SetFramePointer(this->Pop());
		uint64_t ret = this->Pop();

		uint64_t numargs = this->Pop();
		for(int i = 0; i < numargs; i++)
		{
			this->Pop();
		}
		this->cpu->Registers[InstructionPointerIndex] = ret;
	}

	void Decoder::RuntimeCall(Instruction* instr)
	{
		// TODO
	}



















	void Decoder::Push(Instruction* instr)
	{
		this->Push(this->GetOperand(instr, 1));
	}

	void Decoder::Pop(Instruction* instr)
	{
		uint64_t o = this->GetOperand(instr, 3);
		this->cpu->Registers[o] = this->Pop();
	}

	void Decoder::Push(uint64_t v)
	{
		// stack expands downwards.
		// therefore, decrement, then set value.

		this->cpu->Registers[StackPointerIndex] -= WordSize;
		uint64_t stack = this->cpu->Registers[StackPointerIndex];

		this->cpu->GetMemoryManager()->GetController(stack)->Store64(stack, v);
	}

	uint64_t Decoder::Pop()
	{
		// stack expands downwards.
		// therefore, get value, then increment.

		uint64_t stack = this->cpu->Registers[StackPointerIndex];
		uint64_t ret = this->cpu->GetMemoryManager()->GetController(stack)->Load64(stack);
		this->cpu->Registers[StackPointerIndex] += WordSize;

		return ret;
	}

	void Decoder::Stack_Add(Instruction* instr)
	{
		this->Push(this->Pop() + this->Pop());
	}
	void Decoder::Stack_Subtract(Instruction* instr)
	{
		// subtract is order-sensitive.
		// cannot guarantee compiler calls pops in order.
		uint64_t o1 = this->Pop();
		uint64_t o2 = this->Pop();

		this->Push(o1 - o2);
	}

	void Decoder::Stack_Multiply(Instruction* instr)
	{
		this->Push(this->Pop() * this->Pop());
	}

	void Decoder::Stack_Divide(Instruction* instr)
	{
		// divide is order-sensitive.
		// cannot guarantee compiler calls pops in order.
		uint64_t o1 = this->Pop();
		uint64_t o2 = this->Pop();

		this->Push(o1 / o2);
	}

	void Decoder::Stack_Increment(Instruction* instr)
	{
		this->Push(this->Pop() + 1);
	}

	void Decoder::Stack_Decrement(Instruction* instr)
	{
		this->Push(this->Pop() - 1);
	}

	void Decoder::Stack_GreaterThan(Instruction* instr)
	{
		uint64_t o1 = this->Pop();
		uint64_t o2 = this->Pop();
		this->SetResult(this->GetOperand(instr, 3), o1 > o2 ? 1 : 0);
	}
	void Decoder::Stack_LessThan(Instruction* instr)
	{
		uint64_t o1 = this->Pop();
		uint64_t o2 = this->Pop();
		this->SetResult(this->GetOperand(instr, 3), o1 < o2 ? 1 : 0);
	}
	void Decoder::Stack_EqualsTo(Instruction* instr)
	{
		uint64_t o1 = this->Pop();
		uint64_t o2 = this->Pop();
		this->SetResult(this->GetOperand(instr, 3), o1 == o2 ? 1 : 0);
	}
	void Decoder::Stack_GreaterThanOrEqualsTo(Instruction* instr)
	{
		uint64_t o1 = this->Pop();
		uint64_t o2 = this->Pop();
		this->SetResult(this->GetOperand(instr, 3), o1 >= o2 ? 1 : 0);
	}
	void Decoder::Stack_LessThanOrEqualsTo(Instruction* instr)
	{
		uint64_t o1 = this->Pop();
		uint64_t o2 = this->Pop();
		this->SetResult(this->GetOperand(instr, 3), o1 <= o2 ? 1 : 0);
	}



}




