// Pipeline.cpp
// Copyright (c) 2014, zhiayang@gmail.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

#include <stdint.h>
#include <cstdio>
#include <sys/mman.h>
#include <cstring>
#include "VM.hpp"
#include "Utilities.hpp"

namespace VM
{
	InstructionPipeline::InstructionPipeline(CPU* thecpu)
	{
		this->cpu = thecpu;
	}

	Instruction* InstructionPipeline::Fetch()
	{
		uint64_t ip = this->cpu->GetInstructionPointer();
		uint8_t* ptr = this->cpu->GetMemoryManager()->GetController(ip)->GetRawPointer(ip);
		return (Instruction*) (ptr);
	}
}




