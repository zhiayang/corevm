// MemoryController.cpp
// Copyright (c) 2014 - The Foreseeable Future, zhiayang@gmail.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported

#include <cstdio>
#include <stdint.h>
#include <cassert>
#include <sys/mman.h>

#include "Utilities.hpp"
#include "VM.hpp"

namespace VM
{
	MemoryController::MemoryController(uint64_t domainstart, uint64_t domlength, uint64_t latency)
	{
		// todo:
		// implement latency

		this->domstart = domainstart;
		this->domlength = domlength;
		this->latency = latency;

		this->backingstore = (uint8_t*) mmap(0, domlength, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON, -1, 0);
	}

	bool MemoryController::WithinDomain(uint64_t addr)
	{
		return addr >= this->domstart && addr < (this->domstart + this->domlength);
	}

	uint8_t MemoryController::LoadByte(uint64_t addr)
	{
		assert(this->WithinDomain(addr));
		return *(this->backingstore + addr);
	}
	uint16_t MemoryController::Load16(uint64_t addr)
	{
		assert(this->WithinDomain(addr));
		return *((uint16_t*) (this->backingstore + addr));
	}
	uint32_t MemoryController::Load32(uint64_t addr)
	{
		assert(this->WithinDomain(addr));
		return *((uint32_t*) (this->backingstore + addr));
	}
	uint64_t MemoryController::Load64(uint64_t addr)
	{
		assert(this->WithinDomain(addr));
		return *((uint64_t*) (this->backingstore + addr));
	}

	void MemoryController::StoreByte(uint64_t addr, uint8_t val)
	{
		assert(this->WithinDomain(addr));
		*((uint8_t*) (this->backingstore + addr)) = val;
	}
	void MemoryController::Store16(uint64_t addr, uint16_t val)
	{
		assert(this->WithinDomain(addr));
		*((uint16_t*) (this->backingstore + addr)) = val;
	}
	void MemoryController::Store32(uint64_t addr, uint32_t val)
	{
		assert(this->WithinDomain(addr));
		*((uint32_t*) (this->backingstore + addr)) = val;
	}
	void MemoryController::Store64(uint64_t addr, uint64_t val)
	{
		assert(this->WithinDomain(addr));
		*((uint64_t*) (this->backingstore + addr)) = val;
	}






	uint8_t* MemoryController::LoadBytes(uint64_t addr, uint64_t length)
	{
		assert(this->WithinDomain(addr));
		return this->backingstore + addr;
	}

	uint16_t* MemoryController::Load16(uint64_t addr, uint64_t length)
	{
		assert(this->WithinDomain(addr));
		return (uint16_t*) (this->backingstore + addr);
	}
	uint32_t* MemoryController::Load32(uint64_t addr, uint64_t length)
	{
		assert(this->WithinDomain(addr));
		return (uint32_t*) (this->backingstore + addr);
	}
	uint64_t* MemoryController::Load64(uint64_t addr, uint64_t length)
	{
		assert(this->WithinDomain(addr));
		return (uint64_t*) (this->backingstore + addr);
	}

	uint8_t* MemoryController::GetRawPointer(uint64_t addr)
	{
		return this->backingstore + addr;
	}

	void MemoryController::StoreBytes(uint64_t addr, uint8_t* bytes, uint64_t length)
	{
		memcpy(this->backingstore + addr, bytes, length);
	}

	void MemoryController::Store16(uint64_t addr, uint16_t* buf, uint64_t length)
	{
		memcpy(this->backingstore + addr, buf, length * sizeof(uint16_t));
	}

	void MemoryController::Store32(uint64_t addr, uint32_t* buf, uint64_t length)
	{
		memcpy(this->backingstore + addr, buf, length * sizeof(uint32_t));
	}

	void MemoryController::Store64(uint64_t addr, uint64_t* buf, uint64_t length)
	{
		memcpy(this->backingstore + addr, buf, length * sizeof(uint64_t));
	}
}












