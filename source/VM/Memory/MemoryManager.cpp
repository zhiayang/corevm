// MemoryController.cpp
// Copyright (c) 2014 - The Foreseeable Future, zhiayang@gmail.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported

#include <cstdio>
#include <iostream>
#include <stdint.h>
#include "Utilities.hpp"
#include "VM.hpp"
#include <vector>

using namespace std;

namespace VM
{
	MemoryManager::MemoryManager(uint64_t size, vector<MemoryController*> controllers, vector<CacheController*> cachelist)
	{
		this->memsize = size;
		this->memcontrollers = new map<range<uint64_t>, MemoryController*, left_of_range<uint64_t>>();
		this->caches = new vector<CacheController*>();

		// add the controllers to the list.
		for(auto c : controllers)
			(*this->memcontrollers)[range<uint64_t>(c->domstart, c->domstart + c->domlength)] = c;

		for(auto c : cachelist)
			this->caches->push_back(c);
	}

	MemoryController* MemoryManager::GetController(uint64_t addr)
	{
		MemoryController* c = (*this->memcontrollers)[addr];
		if(c->WithinDomain(addr))
			return c;

		return 0;
	}
}


