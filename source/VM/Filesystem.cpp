// Filesystem.cpp
// Copyright (c) 2014 - The Foreseeable Future, zhiayang@gmail.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported

#include <cstdio>
#include <stdint.h>
#include <cassert>
#include <sys/stat.h>

#include "Utilities.hpp"
#include "VM.hpp"

using namespace std;

namespace VM
{
	Filesystem::Filesystem(string rootfs)
	{
		this->rootfs = new string(rootfs);
	}

	uint8_t* Filesystem::ReadFile(string path, uint64_t& s)
	{
		struct stat st;
		stat((*this->rootfs + path).c_str(), &st);
		int64_t size = st.st_size;

		uint8_t* ret = new uint8_t[size]{  };
		s = size;

		FILE* file = fopen((*this->rootfs + path).c_str(), "r");

		fread(ret, 1, size, file);
		fclose(file);
		return ret;
	}
}





