// Utilities.cpp
// Copyright (c) 2014, zhiayang@gmail.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

#include <iostream>
#include <string>
#include <sys/time.h>

#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif

namespace Utilities
{
	std::string& LeftTrimString(std::string& s)
	{
		s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
		return s;
	}

	// trim from end
	std::string& RightTrimString(std::string& s)
	{
		s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
		return s;
	}

	// trim from both ends
	std::string& TrimString(std::string& s)
	{
		return LeftTrimString(RightTrimString(s));
	}

	int __nsleep(const struct timespec *req, struct timespec *rem)
	{
		timespec temp_rem;
		if(nanosleep(req,rem) == -1)
			return __nsleep(rem, &temp_rem);
		else
			return 1;
	}

	void SleepMiliseconds(uint64_t milisec)
	{
		timespec req={ 0 };
		timespec rem = { 0 };

		time_t sec = (int) (milisec / 1000);
		milisec = milisec - (sec * 1000);
		req.tv_sec = sec;
		req.tv_nsec = milisec * 1000000L;
		__nsleep(&req, &rem);
	}

	void SleepNanoseconds(uint64_t nanosec)
	{
		timespec req = { 0 };
		timespec rem = { 0 };

		req.tv_nsec = nanosec;
		__nsleep(&req, &rem);
	}

	uint64_t Timestamp()
	{
		struct timespec ts;

		#ifdef __MACH__ // OS X does not have clock_gettime, use clock_get_time
			clock_serv_t cclock;
			mach_timespec_t mts;
			host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
			clock_get_time(cclock, &mts);
			mach_port_deallocate(mach_task_self(), cclock);
			ts.tv_sec = mts.tv_sec;
			ts.tv_nsec = mts.tv_nsec;
		#else
			clock_gettime(CLOCK_REALTIME, &ts);
		#endif

		return ts.tv_nsec;
	}
}
