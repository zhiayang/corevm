// StringUtil.cpp
// Copyright (c) 2014, zhiayang@gmail.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

#include <iostream>
#include <cmath>
#include <string>

namespace StringUtil
{
	std::string& Format(const char* str, ...)
	{
		char* arr = new char[256];

		va_list args;
		va_start(args, str);

		vsnprintf(arr, 256, str, args);
		std::string* ret = new std::string(arr);

		va_end(args);
		delete[] arr;

		return *ret;
	}

	std::string& ReplaceString(std::string& subject, const std::string& search, const std::string& replace)
	{
		size_t pos = 0;
		while((pos = subject.find(search, pos)) != std::string::npos)
		{
			subject.replace(pos, search.length(), replace);
			pos += replace.length();
		}

		return subject;
	}
}
