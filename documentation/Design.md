### Design of the [core] virtual CPU ###

Due to the virtual nature of the architecture, hardware/cost constraits are not an issue.

1. Vx is primarily a register-based, fully 64-bit RISC architecture.
2. There are 32 GPRs, %r01 -> %r32.
3. The current execution address is stored in the %rip register.
4. There is a special register, %rr, for storing results of arithmetic operations.
5. GPR names are always double digits.
6. While register based, Vx also uses a stack, primarily for recursive calling and storing/restoring register values.


Caller descriptions:
1. There are 16 registers, '%a01 - %a16', meant for passing arguments to functions.
2. Functions requiring more than 16 arguments should look into currying.
3. The number of arguments is placed in %rr.
4. The return value is stored in %rr.


Callee descriptions:
1. Arguments are in %a01 to %a16, as above.
2. The number of arguments is in %rr.
3. The return value MUST be placed in %rr before calling the return instruction.


RCall
1. Runtime call is used only for external applications using the VM.
2. When run in standalone mode, the VM's CPU will ignore rcall instructions. This may change.
3. When hosted, the VM is given a list of libraries (os-specific), and will parse and load them to VM memory.
4. During runtime, the caller string is checked against a hashmap.
5. If a match is found, a function pointer is created, the function is called and returned from, transparently to the VM program.



##### Assembler #####
1. Registers are prefixed with '%'
2. Immediate values to be prefixed with '$'.
3. Getting the value at an address (dereferencing) is done with '(addr)', for example '(%r5)'
4. Offsetting from the address can be done with '(value +/- offset)', eg. (%r21 - 64)
5. Arithmetic operations store the result in %rr by default, however this can be overridden by adding ' -> %rX' after the instruction.
	a. This comes at no extra cost
	b. The contents of the %rr register are preserved.

6. Operations with more than one argument must have them separated with commas.
	eg. add %r1, %r2 -> %r3
	This will add the contents of %r1 with the contents of %r2 and store the result in %r3.


##### Binary Representation #####
1. Currently it's raw binary. Planning on something like ELF.
2. All little endian.

3. Register index 0 is invalid.
4. Register indices 1 - 32 are the GPRs (%r01 - %r32)
5. Register indices 33, 34 and 35 are the instructionpointer, resultregister and stackpointer

format:
OPCODE	(2 bytes)
REGINDX1	(1 byte)
REGINDX2	(1 byte)
OUTINDX	(1 byte)
ZERO		(3 bytes, just zero)
IMM		(8 bytes, padded with zeroes.)






##### VxEF (Vx Executable Format) #####
A very simple executable format, designed specifically for the Vx architecture and tailored to its needs.

A VxEF file consists of a number of parts:
1. Header: this provides information on how to parse the following file.
2. Executable data: this contains the instructions of the program. The size of this portion is specified in the header.
3. Program data: this section mainly stores strings and other nonmutable data.
4. End.
























