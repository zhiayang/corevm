# Written for [core].


CXX		= clang++
LD		= clang++
CXXFLAGS	= -Wall -Isource/include -fPIC -c -std=gnu++1y -stdlib=libc++

ASMSRC	= $(shell find source/Assembler -iname "*.cpp")
ASMOBJ	= $(ASMSRC:.cpp=.cpp.o)
ASMDEPS	= $(ASMSRC:.cpp=.ccp.d)

VMSRC		= $(shell find source/VM -iname "*.cpp")
VMOBJ		= $(VMSRC:.cpp=.cpp.o)
VMDEPS	= $(VMSRC:.cpp=.ccp.d)

UTILSRC	= $(shell find source/Utilities -iname "*.cpp")
UTILOBJ	= $(UTILSRC:.cpp=.cpp.o)
UTILDEPS	= $(UTILSRC:.cpp=.cpp.d)


.DEFAULT_GOAL = asm
-include $(CXXDEPS)
-include $(UTILDEPS)


WARNINGS	= -Wno-padded -Wno-c++98-compat-pedantic -Wno-c++98-compat -Wno-cast-align -Wno-unreachable-code -Wno-gnu -Wno-missing-prototypes -Wno-switch-enum -Wno-packed -Wno-missing-noreturn -Wno-float-equal -Wno-sign-conversion -Wno-old-style-cast

LIBRARIES	=  -lncurses


.PHONY: asm all vm clean

all: asm vm
	@:

vm: output/corevm
	@:

asm: output/cas
	@:

output/corevm: $(VMOBJ) $(UTILOBJ)
	@$(LD) -o output/corevm $(VMOBJ) $(UTILOBJ) $(LIBRARIES)

output/cas: $(ASMOBJ) $(UTILOBJ)
	@$(LD) -o output/cas $(ASMOBJ) $(UTILOBJ)

%.cpp.o: %.cpp
	@$(CXX) $(CXXFLAGS) $(WARNINGS) -MMD -MP -o $@ $<

clean:
	@find source -iname "*cpp.d" | xargs rm
	@find source -iname "*.o" | xargs rm

